{
  description = "Rust Development Overlay";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixpkgs-unstable";
    naersk = { url = "github:nix-community/naersk"; inputs.nixpkgs.follows = "nixpkgs"; };
    flake-utils.url  = "github:numtide/flake-utils";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, fenix, flake-utils, naersk, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlay ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        toolchain = (pkgs.fenix.toolchainOf {
          channel = "stable";
          sha256 = "sha256-KXx+ID0y4mg2B3LHp7IyaiMrdexF6octADnAtFIOjrY=";
        }).withComponents [
          "cargo" "rustc" "clippy" "rustfmt" "rust-src"
        ];

        naersk-lib = naersk.lib."${system}".override {
          rustc = toolchain;
          cargo = toolchain;
        };

        buildInputs = with pkgs; [
          openssl
          pkgconfig
          exa
          fd
          toolchain
          valgrind
          massif-visualizer

          clang_14
          mold

          # deps for eframe
          glib
          pango
          gdk-pixbuf
          atk
          gtk3
          libGL
          libglvnd
          freetype
          vulkan-headers
          vulkan-loader
          vulkan-tools
          wayland
          libxkbcommon
          ffmpeg_5-full
          fontconfig
          expat
        ];

        nativeBuildInputs = with pkgs; [ autoPatchelfHook wrapGAppsHook makeWrapper ];
        ldLibPath = pkgs.lib.makeLibraryPath buildInputs;

        cargo_linker = "${pkgs.clang_14}/bin/clang";
        cargo_rustflags = "-C link-arg=--ld-path=${pkgs.mold}/bin/mold";


      in
        rec {
          packages = {
            deframe = naersk-lib.buildPackage {
              pname = "deframe";
              root = ./.;
              buildInputs = buildInputs;
              nativeBuildInputs = nativeBuildInputs;
              postInstall = ''
              wrapProgram $out/bin/deframe --prefix LD_LIBRARY_PATH : ${ldLibPath}
              '';

              CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER = cargo_linker;
              CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS = cargo_rustflags;
            };

            deframe-dev = naersk-lib.buildPackage {
              enableParallelBuilding = true;
              pname = "deframe-dev";
              root = ./.;
              buildInputs = buildInputs;
              nativeBuildInputs = nativeBuildInputs;
              release = false;
              postInstall = ''
              wrapProgram $out/bin/deframe --prefix LD_LIBRARY_PATH : ${ldLibPath}
              '';

              CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER = cargo_linker;
              CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS = cargo_rustflags;
              RUST_BACKTRACE= "full";
            };
          };
          defaultPackage = packages.deframe;

          apps = {
            deframe = flake-utils.lib.mkApp {
              drv = packages.deframe;
            };
            deframe-dev = flake-utils.lib.mkApp {
              drv = packages.deframe-dev;
              exePath = "/bin/deframe";
            };
          };
          defaultApp = apps.deframe;

          devShell = pkgs.mkShell {
            buildInputs = buildInputs ++ [ pkgs.rust-analyzer-nightly ];
            nativeBuildInputs = nativeBuildInputs;

            shellHook = ''
            export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER="${cargo_linker}"
            export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS="${cargo_rustflags}"
            export LD_LIBRARY_PATH="${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
            '';
          };
        }
    );
}
