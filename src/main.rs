#![cfg_attr(all(not(debug_assertions), target_family = "windows"), windows_subsystem = "windows")]
#[macro_use]
extern crate tracing;

mod loader;
mod model;
mod pngreader;

use anyhow::Context;
use eframe::{
	egui,
	epaint::{ColorImage, TextureHandle},
};
use loader::{DetectionSettings, FrameLoader, Region};
use model::{DupesTextured, Settings};
use std::io::Write;
use std::{
	cell::{Cell, RefCell},
	mem,
	rc::Rc,
	sync::{atomic::AtomicUsize, Arc},
};
use tracing_subscriber::EnvFilter;

use crate::model::{DupeTextured, TexHandle};

fn main() {
	#[cfg(debug_assertions)]
	{
		std::env::set_var("RUST_LOG", "info,wgpu=warn,deframe=trace");
	}

	tracing_subscriber::fmt()
		.with_ansi(true)
		.with_env_filter(EnvFilter::from_default_env())
		.with_thread_names(true)
		.init();

	let egui_opts = eframe::NativeOptions {
		resizable: false,
		initial_window_size: Some(eframe::emath::vec2(1280.0, 720.0)),
		min_window_size: Some(eframe::emath::vec2(1280.0, 720.0)),
		vsync: true,
		renderer: eframe::Renderer::Wgpu,
		follow_system_theme: false,

		..Default::default()
	};

	let v = std::env::current_exe().unwrap();
	error!(pwd=%v.display(),"nix");

	info!("startup");
	eframe::run_native("deframe", egui_opts, Box::new(|cc| Box::new(App::new(cc))));
}

struct App {
	settings: Settings,

	px_thresh: u8,
	rt_thresh: f64,
	cb_x: usize,
	cb_y: usize,
	cb_w: usize,
	cb_h: usize,

	frame_sel_frame: usize,
	frame_sel_dupe: usize,

	graph_line: Option<Rc<Vec<egui::plot::PlotPoint>>>,
	graph_region: Rc<Cell<(f64, f64)>>,
	total_frames_scanned: usize,

	state: AppMode,
}

#[derive(Debug)]
enum AppMode {
	Start,
	Processing { handle: RefCell<Option<loader::LoadDupeProcess>>, framec: Arc<AtomicUsize> },
	Results { endresult: Rc<anyhow::Result<DupesTextured>> },
}

impl App {
	pub fn new(cc: &eframe::CreationContext) -> Self {
		let settings = cc
			.storage
			.and_then(|storage| eframe::get_value(storage, Settings::key()))
			.unwrap_or_default();

		Self {
			settings,
			px_thresh: 0,
			rt_thresh: 1.0,
			state: AppMode::Start,
			frame_sel_frame: 0,
			frame_sel_dupe: 0,
			cb_x: 0,
			cb_y: 0,
			cb_w: 32,
			cb_h: 32,

			graph_line: None,
			graph_region: Rc::new(Cell::new((f64::INFINITY, f64::INFINITY))),
			total_frames_scanned: 0,
		}
	}
}

impl eframe::App for App {
	fn update(&mut self, ctx: &eframe::egui::Context, frame: &mut eframe::Frame) {
		frame.set_window_title("Deframe - Frame Analyzer");

		egui::CentralPanel::default().show(ctx, |ui| {
			ui.heading("Deframe - Frame Analyzer");

			let mut next_state = None;

			match &self.state {
				AppMode::Start => {
					let next = self.render_state_start(ui);
					if let Some((l, f)) = next {
						next_state =
							Some(AppMode::Processing { handle: RefCell::new(Some(l)), framec: f });
					}
				},
				AppMode::Processing { framec, handle } => {
					let frame_count = framec.load(std::sync::atomic::Ordering::Relaxed);
					self.total_frames_scanned = frame_count;
					let cancelled = self.render_state_processing(ui, frame_count);

					if cancelled {
						next_state = Some(AppMode::Start)
					}

					if handle.borrow().as_ref().map(|v| v.is_finished()).unwrap_or(false)
						&& handle.borrow().is_some()
					{
						let handle = mem::take(&mut *handle.borrow_mut()).unwrap();
						let res = handle
							.join()
							.expect("thread died, join failed")
							.context("failed to find dupes")
							.map(|r| {
								let mut transformed = Vec::with_capacity(r.0.len());

								for (dupe_idx, dupe) in r.0.into_iter().enumerate() {
									let mut dupe_textures = Vec::with_capacity(dupe.frames.len());

									for frame in dupe.frames {
										let frame_idx = frame.index;
										let raw_size =
											[frame.frame.width() as _, frame.frame.height() as _];
										let raw_pixels = frame.frame.as_flat_samples();
										let raw_image = ColorImage::from_rgba_unmultiplied(
											raw_size,
											raw_pixels.as_slice(),
										);
										let texture = ctx.load_texture(
											format!("df-d{}-f{}", dupe_idx, frame_idx),
											raw_image,
											egui::TextureFilter::Linear,
										);

										dupe_textures.push(TexHandle(texture, frame_idx));
									}

									transformed.push(DupeTextured {
										idx: dupe_idx,
										start_frame: dupe.start_frame,
										textures: dupe_textures,
									});
								}

								self.graph_line =
									Some(Rc::new(compute_points(&transformed, frame_count)));

								DupesTextured { dupes: transformed }
							});

						next_state = Some(AppMode::Results { endresult: Rc::new(res) });
					}
				},
				AppMode::Results { endresult: res } => {
					self.render_state_results(ui, &*Rc::clone(res))
				},
			}

			if let Some(state) = next_state {
				self.state = state;
			}

			ui.allocate_space(egui::Vec2::new(ui.available_width(), 0.0));
		});
	}

	fn save(&mut self, storage: &mut dyn eframe::Storage) {
		eframe::set_value(storage, Settings::key(), &self.settings);
	}

	fn persist_egui_memory(&self) -> bool {
		false
	}

	fn persist_native_window(&self) -> bool {
		true
	}
}

impl App {
	fn render_state_start(
		&mut self,
		ui: &mut egui::Ui,
	) -> Option<(loader::LoadDupeProcess, Arc<AtomicUsize>)> {
		ui.group(|ui| {
			ui.label("Settings");
			ui.allocate_space(egui::Vec2::new(ui.available_width(), 0.0));


			ui.horizontal(|ui| {
				ui.label("Path to ffmpeg.exe");
				ui.add(egui::TextEdit::singleline(&mut self.settings.ffmpeg_f).desired_width(f32::INFINITY));
			});

			ui.horizontal(|ui| {
				ui.label("Source File");
				ui.add(egui::TextEdit::singleline(&mut self.settings.src_f).desired_width(f32::INFINITY));
			});

			egui::Grid::new("settings-grid").show(ui, |ui| {
				ui.label("Per-Pixel Threshold: ");
				ui.add(egui::DragValue::new(&mut self.px_thresh).clamp_range(0..=255));
				ui.end_row();
				ui.label("Total Threshold: ").on_hover_text("The ratio of different pixels to the full region size at which the full frame is considered different (0 = no ratio at all, just 1 pixel is enough)");
				ui.add(egui::DragValue::new(&mut self.rt_thresh).clamp_range(0..=100).suffix("%"));
				ui.end_row();

				ui.label("Test Region Start (x)").on_hover_text("The pixel position for the start of the test rectangle (from the top left corner)");
				ui.add(egui::DragValue::new(&mut self.cb_x));
				ui.end_row();
				ui.label("Test Region Start (y)").on_hover_text("The pixel position for the start of the test rectangle (from the top left corner)");
				ui.add(egui::DragValue::new(&mut self.cb_y));
				ui.end_row();
				ui.label("Test Region Width (w)").on_hover_text("The pixel width of the text rectangle (bottom right corner will be x + w)");
				ui.add(egui::DragValue::new(&mut self.cb_w));
				ui.end_row();
				ui.label("Test Region Height (h)").on_hover_text("The pixel width of the text rectangle (bottom right corner will be y + h)");
				ui.add(egui::DragValue::new(&mut self.cb_h));
				ui.end_row();
			});

			if ui.button("Scan").clicked() && std::path::Path::new(&self.settings.ffmpeg_f).exists() && std::path::Path::new(&self.settings.src_f).exists() {
					let loader = FrameLoader::new(
					"ffmpeg",
					&self.settings.src_f,
					DetectionSettings {
						region: Region { x: self.cb_x, y: self.cb_y, w: self.cb_w, h: self.cb_h },
						error_thresh_pixel: self.px_thresh,
						error_thresh_ratio: self.rt_thresh,
					},
				);

				let counter = Arc::clone(&loader.current_frame);
				let res = loader.load().unwrap();

				return Some((res, counter));
			}

			None
		})
		.inner
	}

	fn render_state_processing(&self, ui: &mut egui::Ui, framec: usize) -> bool {
		ui.vertical_centered(|ui| {
			ui.label(egui::RichText::new("Scanning...").color(egui::Color32::YELLOW).size(32.0));
			ui.label(
				egui::RichText::new(format!("Current Frame = {}", framec))
					.color(egui::Color32::YELLOW)
					.size(16.0),
			);
			ui.button("Cancel").clicked()
		})
		.inner
	}

	fn render_state_results(&mut self, ui: &mut egui::Ui, res: &anyhow::Result<DupesTextured>) {
		match res {
			Ok(v) => {
				if !v.dupes.is_empty() {
					let dupe = &v.dupes[self.frame_sel_dupe];
					ui.horizontal_top(|ui| {
						egui::Grid::new("deframe-dupe-selector-controls").num_columns(4).show(
							ui,
							|ui| {
								next_before_slider(
									ui,
									&mut self.frame_sel_dupe,
									v.dupes.len(),
									Some("Dupliate: "),
									|| {
										self.frame_sel_frame = 0;
									},
								);
								ui.end_row();
								next_before_slider(
									ui,
									&mut self.frame_sel_frame,
									v.dupes[self.frame_sel_dupe].textures.len(),
									Some("Frame in Duplicate: "),
									|| {},
								);
								ui.end_row();
							},
						);

						ui.add_space(128.0);

						egui::Grid::new("deframe-dupe-frame-info").num_columns(4).show(ui, |ui| {
							ui.label("Dupe: ");
							ui.label(self.frame_sel_dupe.to_string());

							ui.label("Index in Video (Dupe): ");
							ui.label(dupe.start_frame.to_string());

							ui.label("Duplicates% (Total)");
							ui.label(
								((v.dupes
									.iter()
									.map(|v| v.textures.len().saturating_sub(1))
									.sum::<usize>() as f64 / self.total_frames_scanned as f64)
									* 100.0)
									.to_string(),
							);

							ui.end_row();

							ui.label("Frame: ");
							ui.label(self.frame_sel_frame.to_string());

							ui.label("Index in Video (Frame): ");
							ui.label(dupe.textures[self.frame_sel_frame].1.to_string());

							let region = self.graph_region.get();
							ui.label("Duplicates% (Region)");
							let cnt: usize = v
								.dupes
								.iter()
								.filter_map(|vs| {
									vs.textures.last().and_then(|x| {
										if region.0 <= vs.start_frame as f64
											&& x.1 as f64 <= region.1
										{
											Some(vs.textures.len().saturating_sub(1))
										} else {
											None
										}
									})
								})
								.sum();
							let region_size =
								(region.1 - region.0).min(self.total_frames_scanned as f64).floor();
							if cnt == 0 || region_size == 0.0 {
								ui.label(format!("0 ({} frames in region)", region_size));
							} else {
								ui.label(format!(
									"{:.2} ({} frames in region)",
									(cnt as f64 / region_size) * 100.0,
									region_size
								));
							}

							ui.end_row();
						});

						ui.add_space(128.0);

						if ui.button("Export").clicked() {
							let mut file = std::fs::File::create("export.csv").unwrap();
							let mut uidx = 0;

							let mut i = 0;
							while i < self.total_frames_scanned {
								if uidx < v.dupes.len() && v.dupes[uidx].start_frame == i {
									let l = v.dupes[uidx].textures.len();
									writeln!(&mut file, "{};false;", i).unwrap();
									for n in 1..l {
										writeln!(&mut file, "{};true;{}", i + n, i).unwrap();
										i += 1;
									}
									uidx += 1;
								} else {
									writeln!(&mut file, "{};false;", i).unwrap();
									i += 1;
								}
							}
						}
					});

					let data = self.graph_line.as_ref().unwrap().clone();
					let lined = data.to_vec();
					let line = egui::plot::Line::new(egui::plot::PlotPoints::Owned(lined));
					let region_rc = Rc::clone(&self.graph_region);

					egui::plot::Plot::new("deframe-dupe-map-graph")
						.height(80.0)
						.label_formatter(|_, _| "".to_owned())
						.coordinates_formatter(
							egui::plot::Corner::LeftTop,
							egui::plot::CoordinatesFormatter::new(move |v, r| {
								region_rc.set((r.min()[0], r.max()[0]));
								let x = v.x.floor() as usize;

								if x >= data.len() {
									String::from("frame: ????? - dupe?: ???")
								} else {
									format!(
										"frame: {:>5} - dupe?: {}",
										x,
										if data[x].y.floor() > 0.0 { "yes" } else { "no" }
									)
								}
							}),
						)
						.include_y(2.0)
						.show_y(false)
						.show(ui, |plot_ui| {
							plot_ui.line(line);
						});

					ui.separator();

					let handle: &TextureHandle = &dupe.textures[self.frame_sel_frame];
					ui.image(handle, ui.available_size());
				} else {
					ui.vertical_centered(|ui| {
						ui.label(
							egui::RichText::new("No Dupes Found!")
								.color(egui::Color32::LIGHT_GREEN)
								.size(32.0),
						);
					});
				}
			},
			Err(why) => {
				ui.vertical_centered(|ui| {
					ui.label(
						egui::RichText::new("Error").color(egui::Color32::LIGHT_RED).size(32.0),
					);
					ui.label(format!("{:?}", why));
				});
			},
		}
	}
}

fn next_before_slider(
	ui: &mut egui::Ui,
	val: &mut usize,
	slmax: usize,
	label: Option<&'static str>,
	onchange: impl FnOnce(),
) {
	if let Some(label_text) = label {
		ui.label(label_text);
	}

	let mut changed = false;

	ui.horizontal(|ui| {
		let btn_left = ui.button("<");
		let btn_right = ui.button(">");

		if btn_left.clicked() {
			*val = val.saturating_sub(1);
			changed = true;
		}
		if btn_right.clicked() {
			*val = (*val + 1).min(slmax - 1);
			changed = true;
		}
	});

	let slider = ui.add(egui::Slider::new(val, 0..=slmax - 1));

	changed = changed || slider.changed();
	if changed {
		(onchange)();
	}
}

fn compute_points(d: &[DupeTextured], frames_v: usize) -> Vec<egui::plot::PlotPoint> {
	let mut uidx = 0;
	let mut points = Vec::new();

	let mut i = 0;
	while i < frames_v {
		if uidx < d.len() && d[uidx].start_frame == i {
			for _ in 0..d[uidx].textures.len() {
				points.push(egui::plot::PlotPoint::new(i as f64, 1.0));
				i += 1;
			}
			uidx += 1;
		} else {
			points.push(egui::plot::PlotPoint::new(i as f64, 0.0));
			i += 1;
		}
	}

	points
}
