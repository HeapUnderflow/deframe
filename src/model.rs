use std::{
	fmt,
	ops::{Deref, DerefMut},
};

use eframe::epaint::TextureHandle;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Settings {
	pub src_f: String,
	pub ffmpeg_f: String,
}

impl Settings {
	pub fn key() -> &'static str {
		concat!(env!("CARGO_PKG_NAME"), "-settings")
	}
}

impl Default for Settings {
	fn default() -> Self {
		Self { ffmpeg_f: String::from("ffmpeg"), src_f: String::new() }
	}
}

#[derive(Debug)]
pub struct DupesTextured {
	pub dupes: Vec<DupeTextured>,
}

#[derive(Debug)]
pub struct DupeTextured {
	pub idx: usize,
	pub start_frame: usize,
	pub textures: Vec<TexHandle>,
}

pub struct TexHandle(pub TextureHandle, pub usize);

impl Deref for TexHandle {
	type Target = TextureHandle;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for TexHandle {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl Into<TextureHandle> for TexHandle {
	fn into(self) -> TextureHandle {
		self.0
	}
}

impl fmt::Debug for TexHandle {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		f.debug_tuple("TexHandle")
			.field(&format_args!("[egui::TextureHandle->{:p}]", &self.0 as *const _))
			.finish()
	}
}
