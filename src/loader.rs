use crate::pngreader::PngReader;
use anyhow::Context;
use std::{
	fmt, mem,
	process::{Command, Stdio},
	sync::{atomic::AtomicUsize, Arc},
	thread::{self, JoinHandle},
};

macro_rules! ffarg {
	($c:ident, $arg:expr) => {{
		(&mut $c).arg($arg);
	}};
	($c:ident, $arg:expr, $val:expr) => {{
		(&mut $c).arg($arg).arg($val);
	}};
}

#[derive(Debug, Clone)]
pub struct Region {
	pub x: usize,
	pub y: usize,
	pub w: usize,
	pub h: usize,
}

impl Region {
	#[inline]
	pub fn size(&self) -> usize {
		self.w * self.h
	}

	pub fn pixel_at<'p>(&self, f: &'p Frame, x: usize, y: usize) -> Option<&'p image::Rgba<u8>> {
		if x >= self.w || y >= self.h {
			return None;
		}

		Some(f.get_pixel((self.x + x) as u32, (self.y + y) as u32))
	}

	pub fn clamp_to(&mut self, tw: usize, th: usize) {
		if self.x + self.w > tw {
			self.w = tw.saturating_sub(self.x);
		}
		if self.y * self.h > th {
			self.h = th.saturating_sub(self.y);
		}
	}
}

#[derive(Debug)]
pub struct FrameLoader {
	ffmpeg: String,
	source: String,

	pub current_frame: Arc<AtomicUsize>,
	dts: DetectionSettings,
}

#[derive(Debug, Clone)]
pub struct DetectionSettings {
	pub region: Region,
	pub error_thresh_pixel: u8,
	pub error_thresh_ratio: f64,
}

pub type LoadDupeProcess = JoinHandle<anyhow::Result<Dupes>>;

impl FrameLoader {
	pub fn new(ff: &str, source: &str, settings: DetectionSettings) -> Self {
		Self {
			ffmpeg: ff.to_owned(),
			source: source.to_owned(),
			dts: settings,

			current_frame: Arc::new(AtomicUsize::new(0)),
		}
	}

	pub fn load(self) -> anyhow::Result<LoadDupeProcess> {
		let mut process = Command::new(self.ffmpeg);
		ffarg!(process, "-i", self.source);
		ffarg!(process, "-map", "0:v:0");
		ffarg!(process, "-c:v", "png");
		ffarg!(process, "-f", "image2pipe");
		ffarg!(process, "-y");
		ffarg!(process, "-");

		let proc =
			process.stdout(Stdio::piped()).stderr(Stdio::null()).stdin(Stdio::null()).spawn()?;

		let settings = self.dts.clone();
		let current_frame = Arc::clone(&self.current_frame);

		let handle =
			thread::Builder::new().name(String::from("ffmpeg-png-loader")).spawn(move || {
				let mut settings = settings;
				let mut pngs = PngReader::new(proc.stdout.unwrap());
				let frame_it = std::iter::from_fn(|| pngs.next_png().transpose());

				let mut dupes = Vec::new();

				let mut last_frame = None;
				let mut cdup = DupeCollector::default();
				for (idx, png) in frame_it.enumerate() {
					current_frame.store(idx, std::sync::atomic::Ordering::Relaxed);
					let new = png.context("failed to load png")?;

					let last = match mem::replace(&mut last_frame, Some(new.clone())) {
						Some(frame) => frame,
						None => {
							settings.region.clamp_to(new.width() as _, new.height() as _);
							last_frame = Some(new);
							continue;
						},
					};

					trace!(%idx, "frame");

					if Self::frames_are_dupe(&settings, &last, &new) {
						if !cdup.has_dupes() {
							cdup.push_dup(AnnotatedFrame { frame: last, index: idx - 1 });
						}
						cdup.push_dup(AnnotatedFrame { frame: new, index: idx });
					} else if cdup.has_dupes() {
						dupes.push(mem::take(&mut cdup).into_dupe().unwrap());
					}
				}

				anyhow::Result::Ok(Dupes(dupes))
			});

		handle.context("handle")
	}

	fn frames_are_dupe(
		settings: &DetectionSettings,
		last_frame: &Frame,
		current_frame: &Frame,
	) -> bool {
		const R: usize = 0;
		const G: usize = 1;
		const B: usize = 2;

		macro_rules! some_or_ret {
			($e:expr, $n:expr) => {
				match $e {
					Some(v) => v,
					None => {
						trace!(expr=?stringify!($e), "expr was none");
						return $n
					}
				}
			};
		}

		fn check_thresh(delta: u8, left: u8, right: u8) -> bool {
			left == right
				|| (left.saturating_sub(delta) <= right && left.saturating_add(delta) >= right)
				|| (right.saturating_sub(delta) <= left && right.saturating_add(delta) >= left)
		}

		let mut samey_pixels = 0u32;
		let mut out = Vec::new();
		for y in 0..settings.region.h {
			for x in 0..settings.region.w {
				let px0 = some_or_ret!(settings.region.pixel_at(last_frame, x, y), false);
				let px1 = some_or_ret!(settings.region.pixel_at(current_frame, x, y), false);

				let samey = check_thresh(settings.error_thresh_pixel, px0[R], px1[R])
					&& check_thresh(settings.error_thresh_pixel, px0[G], px1[G])
					&& check_thresh(settings.error_thresh_pixel, px0[B], px1[B]);

				if samey {
					samey_pixels += 1;
				}

				out.push((px0[R], px1[R]));
			}
		}

		trace!(%samey_pixels);
		if samey_pixels == 0 {
			trace!("0 dupe pixels");
			return false;
		}

		let ft = settings.region.size() as f64 / samey_pixels as f64;
		trace!(ratio.found=%ft, ratio.thresh=%settings.error_thresh_ratio, "region ratio");
		settings.error_thresh_ratio != 0.0 && ft <= settings.error_thresh_ratio
	}
}

pub type Frame = crate::pngreader::Image;
pub struct AnnotatedFrame {
	pub frame: Frame,
	pub index: usize,
}

impl fmt::Debug for AnnotatedFrame {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		f.debug_struct("AnnotatedFrame")
			.field("frame", &self.frame)
			.field("index", &self.index)
			.finish()
	}
}

pub struct DupeCollector {
	frames: Vec<AnnotatedFrame>,
	first_dup: Option<usize>,
}

impl Default for DupeCollector {
	fn default() -> Self {
		DupeCollector { frames: Vec::new(), first_dup: None }
	}
}

impl DupeCollector {
	fn has_dupes(&self) -> bool {
		self.first_dup.is_some()
	}
	fn push_dup(&mut self, frame: AnnotatedFrame) {
		if self.first_dup.is_none() {
			self.first_dup = Some(frame.index);
		}

		self.frames.push(frame);
	}

	fn into_dupe(self) -> Option<Dupe> {
		let idx = self.first_dup?;
		Some(Dupe { start_frame: idx, frames: self.frames })
	}
}

#[derive(Debug)]
pub struct Dupes(pub Vec<Dupe>);

#[derive(Debug)]
pub struct Dupe {
	pub start_frame: usize,
	pub frames: Vec<AnnotatedFrame>,
}
