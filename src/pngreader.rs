use std::{io, mem};

/// Buffer size for the read buffer
const PNG_BUF_SIZE: usize = 1024 * 64;
/// Initial size of each vector that holds an png
const INITIAL_VEC_SIZE: usize = 1024 * 100;

pub type Image = image::ImageBuffer<image::Rgba<u8>, Vec<u8>>;

pub struct PngReader<T>
where
	T: io::Read,
{
	inner: T,
	holdover: Vec<u8>,
	first: bool,

	fused: bool,
}

impl<T> PngReader<T>
where
	T: io::Read,
{
	/// PNG-Format Magic Bytes
	pub const MARKER: [u8; 8] = [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A];

	pub fn new(r: T) -> PngReader<T> {
		Self { inner: r, holdover: Vec::with_capacity(INITIAL_VEC_SIZE), first: true, fused: false }
	}

	/// Finds Self::MARKER in the given slice, returning the index of the MARKER[0] byte.
	#[inline]
	fn find_marker(b: &[u8]) -> Option<usize> {
		memchr::memmem::find(b, &Self::MARKER)
	}

	pub fn next_png_raw(&mut self) -> io::Result<Option<Vec<u8>>> {
		if self.fused {
			return Ok(None);
		}

		let mut pngbuf = mem::replace(&mut self.holdover, Vec::with_capacity(INITIAL_VEC_SIZE));
		let mut buf = [0u8; PNG_BUF_SIZE];

		loop {
			let r = self.inner.read(&mut buf)?;

			if r == 0 {
				break;
			}

			match Self::find_marker(&buf[..r]) {
				None => {
					if pngbuf.len() > 8 {
						let mut checkchunk = [0u8; 16];
						checkchunk[0..8].copy_from_slice(&pngbuf[pngbuf.len() - 8..]);
						checkchunk[8..(r + 8).min(16)].copy_from_slice(&buf[..r.min(8)]);

						if let Some(chk) = Self::find_marker(&checkchunk) {
							let new_png_len = pngbuf.len() - 8 + chk;
							self.holdover.extend_from_slice(&pngbuf[new_png_len..]);
							pngbuf.truncate(new_png_len);
							self.holdover.extend_from_slice(&buf[..r]);
							break;
						} else {
							pngbuf.extend_from_slice(&buf[..r]);
						}
					} else {
						pngbuf.extend_from_slice(&buf[..r]);
					}
				},
				Some(n) => {
					pngbuf.extend_from_slice(&buf[..n]);
					self.holdover.extend_from_slice(&buf[n..r]);
					break;
				},
			}
		}

		if pngbuf.is_empty() {
			if self.first {
				self.first = false;
				return self.next_png_raw();
			}

			self.fused = true;

			return Ok(None);
		}

		Ok(Some(pngbuf))
	}

	pub fn next_png(&mut self) -> io::Result<Option<Image>> {
		let data = self.next_png_raw()?;
		data.map(|v| {
			trace!(len=%v.len(), "loaded raw png");
			image::load_from_memory_with_format(&v, image::ImageFormat::Png)
				.map(|v| v.to_rgba8())
				.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
		})
		.transpose()
	}
}
